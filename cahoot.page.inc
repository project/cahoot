<?php

/**
 * @file
 * Contains cahoot.page.inc.
 *
 * Page callback for Cahoot entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Cahoot templates.
 *
 * Default template: cahoot.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_cahoot(array &$variables) {
  // Fetch Cahoot Entity Object.
  $cahoot = $variables['elements']['#cahoot'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
