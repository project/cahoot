<?php

namespace Drupal\cahoot;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\cahoot\Entity\CahootInterface;

/**
 * Defines the storage handler class for Cahoot entities.
 *
 * This extends the base storage class, adding required special handling for
 * Cahoot entities.
 *
 * @ingroup cahoot
 */
class CahootStorage extends SqlContentEntityStorage implements CahootStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(CahootInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {cahoot_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {cahoot_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(CahootInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {cahoot_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('cahoot_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
