<?php

namespace Drupal\cahoot;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Cahoot entity.
 *
 * @see \Drupal\cahoot\Entity\Cahoot.
 */
class CahootAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\cahoot\Entity\CahootInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished cahoot entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published cahoot entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit cahoot entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete cahoot entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add cahoot entities');
  }

}
