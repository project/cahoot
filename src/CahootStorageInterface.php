<?php

namespace Drupal\cahoot;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\cahoot\Entity\CahootInterface;

/**
 * Defines the storage handler class for Cahoot entities.
 *
 * This extends the base storage class, adding required special handling for
 * Cahoot entities.
 *
 * @ingroup cahoot
 */
interface CahootStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Cahoot revision IDs for a specific Cahoot.
   *
   * @param \Drupal\cahoot\Entity\CahootInterface $entity
   *   The Cahoot entity.
   *
   * @return int[]
   *   Cahoot revision IDs (in ascending order).
   */
  public function revisionIds(CahootInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Cahoot author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Cahoot revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\cahoot\Entity\CahootInterface $entity
   *   The Cahoot entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(CahootInterface $entity);

  /**
   * Unsets the language for all Cahoot with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
