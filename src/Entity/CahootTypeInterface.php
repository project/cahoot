<?php

namespace Drupal\cahoot\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Cahoot type entities.
 */
interface CahootTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
