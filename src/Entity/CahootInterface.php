<?php

namespace Drupal\cahoot\Entity;

use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Cahoot entities.
 *
 * @ingroup cahoot
 */
interface CahootInterface extends RevisionableInterface, RevisionLogInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Cahoot name.
   *
   * @return string
   *   Name of the Cahoot.
   */
  public function getName();

  /**
   * Sets the Cahoot name.
   *
   * @param string $name
   *   The Cahoot name.
   *
   * @return \Drupal\cahoot\Entity\CahootInterface
   *   The called Cahoot entity.
   */
  public function setName($name);

  /**
   * Gets the Cahoot creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Cahoot.
   */
  public function getCreatedTime();

  /**
   * Sets the Cahoot creation timestamp.
   *
   * @param int $timestamp
   *   The Cahoot creation timestamp.
   *
   * @return \Drupal\cahoot\Entity\CahootInterface
   *   The called Cahoot entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Cahoot published status indicator.
   *
   * Unpublished Cahoot are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Cahoot is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Cahoot.
   *
   * @param bool $published
   *   TRUE to set this Cahoot to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\cahoot\Entity\CahootInterface
   *   The called Cahoot entity.
   */
  public function setPublished($published);

  /**
   * Gets the Cahoot revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Cahoot revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\cahoot\Entity\CahootInterface
   *   The called Cahoot entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Cahoot revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Cahoot revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\cahoot\Entity\CahootInterface
   *   The called Cahoot entity.
   */
  public function setRevisionUserId($uid);

}
