<?php

namespace Drupal\cahoot\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Cahoot entity.
 *
 * @ingroup cahoot
 *
 * @ContentEntityType(
 *   id = "cahoot",
 *   label = @Translation("Cahoot"),
 *   bundle_label = @Translation("Cahoot type"),
 *   handlers = {
 *     "storage" = "Drupal\cahoot\CahootStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\cahoot\CahootListBuilder",
 *     "views_data" = "Drupal\cahoot\Entity\CahootViewsData",
 *     "translation" = "Drupal\cahoot\CahootTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\cahoot\Form\CahootForm",
 *       "add" = "Drupal\cahoot\Form\CahootForm",
 *       "edit" = "Drupal\cahoot\Form\CahootForm",
 *       "delete" = "Drupal\cahoot\Form\CahootDeleteForm",
 *     },
 *     "access" = "Drupal\cahoot\CahootAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\cahoot\CahootHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "cahoot",
 *   data_table = "cahoot_field_data",
 *   revision_table = "cahoot_revision",
 *   revision_data_table = "cahoot_field_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer cahoot entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/content/cahoot/{cahoot}",
 *     "add-page" = "/admin/content/cahoot/add",
 *     "add-form" = "/admin/content/cahoot/add/{cahoot_type}",
 *     "edit-form" = "/admin/content/cahoot/{cahoot}/edit",
 *     "delete-form" = "/admin/content/cahoot/{cahoot}/delete",
 *     "version-history" = "/admin/content/cahoot/{cahoot}/revisions",
 *     "revision" = "/admin/content/cahoot/{cahoot}/revisions/{cahoot_revision}/view",
 *     "revision_revert" = "/admin/content/cahoot/{cahoot}/revisions/{cahoot_revision}/revert",
 *     "translation_revert" = "/admin/content/cahoot/{cahoot}/revisions/{cahoot_revision}/revert/{langcode}",
 *     "revision_delete" = "/admin/content/cahoot/{cahoot}/revisions/{cahoot_revision}/delete",
 *     "collection" = "/admin/content/cahoot",
 *   },
 *   bundle_entity_type = "cahoot_type",
 *   field_ui_base_route = "entity.cahoot_type.edit_form"
 * )
 */
class Cahoot extends RevisionableContentEntityBase implements CahootInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly, make the cahoot owner the
    // revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Cahoot entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Cahoot entity.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the Cahoot is published.'))
      ->setRevisionable(TRUE)
      ->setDefaultValue(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Revision translation affected'))
      ->setDescription(t('Indicates if the last edit of a translation belongs to current revision.'))
      ->setReadOnly(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    return $fields;
  }

}
