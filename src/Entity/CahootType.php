<?php

namespace Drupal\cahoot\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Cahoot type entity.
 *
 * @ConfigEntityType(
 *   id = "cahoot_type",
 *   label = @Translation("Cahoot type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\cahoot\CahootTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\cahoot\Form\CahootTypeForm",
 *       "edit" = "Drupal\cahoot\Form\CahootTypeForm",
 *       "delete" = "Drupal\cahoot\Form\CahootTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\cahoot\CahootTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "cahoot_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "cahoot",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/cahoot_type/{cahoot_type}",
 *     "add-form" = "/admin/structure/cahoot_type/add",
 *     "edit-form" = "/admin/structure/cahoot_type/{cahoot_type}/edit",
 *     "delete-form" = "/admin/structure/cahoot_type/{cahoot_type}/delete",
 *     "collection" = "/admin/structure/cahoot_type"
 *   }
 * )
 */
class CahootType extends ConfigEntityBundleBase implements CahootTypeInterface {

  /**
   * The Cahoot type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Cahoot type label.
   *
   * @var string
   */
  protected $label;

}
