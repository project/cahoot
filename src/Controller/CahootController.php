<?php

namespace Drupal\cahoot\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\cahoot\Entity\CahootInterface;

/**
 * Class CahootController.
 *
 *  Returns responses for Cahoot routes.
 *
 * @package Drupal\cahoot\Controller
 */
class CahootController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Displays a Cahoot  revision.
   *
   * @param int $cahoot_revision
   *   The Cahoot  revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($cahoot_revision) {
    $cahoot = $this->entityManager()->getStorage('cahoot')->loadRevision($cahoot_revision);
    $view_builder = $this->entityManager()->getViewBuilder('cahoot');

    return $view_builder->view($cahoot);
  }

  /**
   * Page title callback for a Cahoot  revision.
   *
   * @param int $cahoot_revision
   *   The Cahoot  revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($cahoot_revision) {
    $cahoot = $this->entityManager()->getStorage('cahoot')->loadRevision($cahoot_revision);
    return $this->t('Revision of %title from %date', ['%title' => $cahoot->label(), '%date' => format_date($cahoot->getRevisionCreationTime())]);
  }

  /**
   * Generates an overview table of older revisions of a Cahoot .
   *
   * @param \Drupal\cahoot\Entity\CahootInterface $cahoot
   *   A Cahoot  object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(CahootInterface $cahoot) {
    $account = $this->currentUser();
    $langcode = $cahoot->language()->getId();
    $langname = $cahoot->language()->getName();
    $languages = $cahoot->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $cahoot_storage = $this->entityManager()->getStorage('cahoot');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $cahoot->label()]) : $this->t('Revisions for %title', ['%title' => $cahoot->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all cahoot revisions") || $account->hasPermission('administer cahoot entities')));
    $delete_permission = (($account->hasPermission("delete all cahoot revisions") || $account->hasPermission('administer cahoot entities')));

    $rows = [];

    $vids = $cahoot_storage->revisionIds($cahoot);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\cahoot\CahootInterface $revision */
      $revision = $cahoot_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = \Drupal::service('date.formatter')->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $cahoot->getRevisionId()) {
          $link = $this->l($date, new Url('entity.cahoot.revision', ['cahoot' => $cahoot->id(), 'cahoot_revision' => $vid]));
        }
        else {
          $link = $cahoot->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => \Drupal::service('renderer')->renderPlain($username),
              'message' => ['#markup' => $revision->getRevisionLogMessage(), '#allowed_tags' => Xss::getHtmlTagList()],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.cahoot.translation_revert', ['cahoot' => $cahoot->id(), 'cahoot_revision' => $vid, 'langcode' => $langcode]) :
              Url::fromRoute('entity.cahoot.revision_revert', ['cahoot' => $cahoot->id(), 'cahoot_revision' => $vid]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.cahoot.revision_delete', ['cahoot' => $cahoot->id(), 'cahoot_revision' => $vid]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['cahoot_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
