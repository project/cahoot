<?php

namespace Drupal\cahoot\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Cahoot entities.
 *
 * @ingroup cahoot
 */
class CahootDeleteForm extends ContentEntityDeleteForm {


}
